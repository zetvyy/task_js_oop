class Animal {
  static isLive = true;
  constructor(name, legs) {
    this.name = name;
    this.legs = legs;
  }

  static canBreathe(name, breathe) {
    console.log(`${name} can breathe with ${breathe}`);
  }

  walk() {
    console.log(`${this.name} walks with ${this.legs} legs`);
  }
}

class Rabbit extends Animal {
  static isMammal = true;
  constructor(name, food, habitat) {
    super(name);
    this.food = food;
    this.habitat = habitat;
  }

  eat() {
    console.log(`${this.name} is an ${this.food} animal`);
  }

  live() {
    console.log(`${this.name} live on ${this.habitat}`);
  }
}

// instansiasi class
let animalA = new Animal("White Rabbit", 4);
animalA.walk();

let rabbit = new Rabbit("White Rabbit", "Herbivora", "land");
rabbit.eat();
rabbit.live();

// nampilkan static property
console.log(Animal.isLive);
console.log(Rabbit.isMammal);

// nampilkan static method
Animal.canBreathe("White Rabbit", "lunge");
